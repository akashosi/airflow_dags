import pytest
from airflow.models import DagBag
from airflow.utils.dag_cycle_tester import check_cycle

@pytest.fixture
def dagbag():
    return DagBag(dag_folder='DAGs/', include_examples=False)


def test_dag_loaded(dagbag):
    dag = dagbag.get_dag(dag_id="validate_ssl_certificates")
    assert dagbag.import_errors == {}
    assert dag is not None
    assert len(dag.tasks) == 1

def test_dag_integrity(dagbag):
    dag = dagbag.get_dag(dag_id="validate_ssl_certificates")
    check_cycle(dag)