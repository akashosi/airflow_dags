import airflow
from airflow import DAG
from datetime import timedelta
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.models import Variable

env_vars = {
	"DB_NAME": Variable.get("AIRFLOW_VAR_DB_NAME", default_var="postgres"),
	"DB_USER": Variable.get("AIRFLOW_VAR_DB_USER",default_var=None),
	"DB_PASSWORD": Variable.get("AIRFLOW_VAR_DB_PASSWORD",default_var=None),
	"DB_HOST": Variable.get("AIRFLOW_VAR_DB_HOST",default_var=None),
}

default_args = {
	'owner': 'airflow',
	'depends_on_past': False,
	'start_date': airflow.utils.dates.days_ago(4),
	'email': ['akashosi@scalhive.com'],
	'email_on_failure': False,
	'email_on_retry': False,
	'retries': 1,
	'retry_delay': timedelta(minutes=5)
}

dag = DAG(
	'validate_ssl_certificates', 
	default_args=default_args, 
	schedule_interval="@daily"
	)


validate_hostnames = KubernetesPodOperator(
	namespace='airflow-datahub',
	image="registry.gitlab.com/akashosi/sslvalidator:latest",
	name="validate-hostnames",
	cmds=["python","check_ssl.py"],
	task_id="validate-hostnames-task",
	env_vars = env_vars,
	get_logs=True,
	dag=dag
	)

validate_hostnames
